package Persona;
public class TestPersona{
	public static void main(String[] args){
		
		/*----Instanciación de objetos----*/

		//Persona 1 (p1)
		Persona p1 = new Persona();
		p1.setId(1);
		p1.setTipoId("C.C");
		p1.setPrimerNombre("José");
		p1.setSegundoNombre("Carlos");
		p1.setPrimerApellido("Ortiz");
		p1.setSegundoApellido("Romero");
		p1.setGenero("Masculino");
		p1.setNacionalidad("Colombiano");
		p1.setEstadoCivil("Soltero");
		p1.setDiaNac(22);
		p1.setMesNac(04);
		p1.setAnioNac(1990);
		p1.setEstatura(1.69);
		p1.setTipoSangre("A");
		p1.setRh("+");

		//Persona 2 (p2)
		Persona p2 = new Persona("Juan", "Jiménez");
		p2.setId(2);
		p2.setTipoId("C.E");
		p2.setSegundoNombre("Antonio");
		p2.setSegundoApellido("Roa");
		p2.setGenero("Masculino");
		p2.setNacionalidad("Chileno");
		p2.setEstadoCivil("Casado");
		p2.setDiaNac(31);
		p2.setMesNac(8);
		p2.setAnioNac(1987);
		p2.setEstatura(1.72);
		p2.setTipoSangre("O");
		p2.setRh("-");

		//Persona 3 (p3)
		Persona p3 = new Persona(3, "T.I", "Laura", "Cruz", "Femenino", "Colombiana");
		p3.setSegundoNombre("Mireya");
		p3.setSegundoApellido("Torres");
		p3.setEstadoCivil("Soltera");
		p3.setDiaNac(26);
		p3.setMesNac(06);
		p3.setAnioNac(2002);
		p3.setEstatura(1.67);
		p3.setTipoSangre("O");
		p3.setRh("+");

		//Persona 4 (p4)
		Persona p4 = new Persona(4, "C.C", "Mario", "Aníbal", "Rodríguez", "Sarmiento", "Masculino", "Colombiano", "Casado", 06, 02, 1996, 2.0, "O", "+");

		/*----Impresión de datos de objetos----*/
		
		System.out.println("Persona 1");
		System.out.println("ID: "+p1.getId()+".\nTipo ID: "+p1.getTipoId()+".");
		System.out.println("Primer nombre: "+p1.getPrimerNombre()+".\nSegundo nombre: "+p1.getSegundoNombre()+".");
		System.out.println("Primer apellido: "+p1.getPrimerApellido()+".\nSegundo apelllido: "+p1.getSegundoApellido()+".");
		System.out.println("Género: "+p1.getGenero()+".\nNacionalidad: "+p1.getNacionalidad()+".\nEstado civil: "+p1.getEstadoCivil()+".");
		System.out.println("Fecha de nacimiento: "+p1.getDiaNac()+"/"+p1.getMesNac()+"/"+p1.getAnioNac()+".");
		System.out.println("Estatura: "+p1.getEstatura()+".\nTipo de sangre: "+p1.getTipoSangre()+".\nRH: "+p1.getRh()+".\n");

		System.out.println("Persona 2");
		System.out.println("ID: "+p2.getId()+".\nTipo ID: "+p2.getTipoId()+".");
		System.out.println("Primer nombre: "+p2.getPrimerNombre()+".\nSegundo nombre: "+p2.getSegundoNombre()+".");
		System.out.println("Primer apellido: "+p2.getPrimerApellido()+".\nSegundo apelllido: "+p2.getSegundoApellido()+".");
		System.out.println("Género: "+p2.getGenero()+".\nNacionalidad: "+p2.getNacionalidad()+".\nEstado civil: "+p2.getEstadoCivil()+".");
		System.out.println("Fecha de nacimiento: "+p2.getDiaNac()+"/"+p2.getMesNac()+"/"+p2.getAnioNac()+".");
		System.out.println("Estatura: "+p2.getEstatura()+".\nTipo de sangre: "+p2.getTipoSangre()+".\nRH: "+p2.getRh()+".\n");

		
		System.out.println("Persona 3");
		System.out.println("ID: "+p3.getId()+".\nTipo ID: "+p3.getTipoId()+".");
		System.out.println("Primer nombre: "+p3.getPrimerNombre()+".\nSegundo nombre: "+p3.getSegundoNombre()+".");
		System.out.println("Primer apellido: "+p3.getPrimerApellido()+".\nSegundo apelllido: "+p3.getSegundoApellido()+".");
		System.out.println("Género: "+p3.getGenero()+".\nNacionalidad: "+p3.getNacionalidad()+".\nEstado civil: "+p3.getEstadoCivil()+".");
		System.out.println("Fecha de nacimiento: "+p3.getDiaNac()+"/"+p3.getMesNac()+"/"+p3.getAnioNac()+".");
		System.out.println("Estatura: "+p3.getEstatura()+".\nTipo de sangre: "+p3.getTipoSangre()+".\nRH: "+p3.getRh()+".\n");


		System.out.println("Persona 4");
		System.out.println("ID: "+p4.getId()+".\nTipo ID: "+p4.getTipoId()+".");
		System.out.println("Primer nombre: "+p4.getPrimerNombre()+".\nSegundo nombre: "+p4.getSegundoNombre()+".");
		System.out.println("Primer apellido: "+p4.getPrimerApellido()+".\nSegundo apelllido: "+p4.getSegundoApellido()+".");
		System.out.println("Género: "+p4.getGenero()+".\nNacionalidad: "+p4.getNacionalidad()+".\nEstado civil: "+p4.getEstadoCivil()+".");
		System.out.println("Fecha de nacimiento: "+p4.getDiaNac()+"/"+p4.getMesNac()+"/"+p4.getAnioNac()+".");
		System.out.println("Estatura: "+p4.getEstatura()+".\nTipo de sangre: "+p4.getTipoSangre()+".\nRH: "+p4.getRh()+".\n");
	}
}
