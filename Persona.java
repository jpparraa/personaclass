package Persona;
/**
* <b>Clase:</b> 		Persona.java<br>
*
* <b>Objetivo:</b>		Desarrollar una clase Persona,
*				conocer la sintaxis, semántica y uso de la documentación
*				de manera correcta en Java.<br>
*
* <b>Asignatura:</b> 		Programación Orientada a Objetos.<br>
* <!--author:			WillyMEN-->
* <!--version: 			1.0-->
**/

//En la plantilla para versión y author se utiliza @ ¿para qué sirve esto?

public class Persona{
	

	//Atributos
	/** ID del objeto.*/			protected int id;
	/** Tipo de ID del objeto.*/		protected String tipoId;
	/** Primer nombre del objeto.*/		protected String primerNombre;
	/** Segundo nombre del objeto.*/	protected String segundoNombre;
	/** Primer apellido del objeto.*/	protected String primerApellido;
	/** Segundo apellido del objeto.*/	protected String segundoApellido;
	/** Género del objeto.*/		protected String genero;
	/** Nacionalidad del objeto.*/		protected String nacionalidad;
	/** Estado civil del objeto.*/		protected String estadoCivil;
	/** Día de nacimiento del objeto.*/	protected int diaNac;
	/** Mes de nacimiento del objeto.*/	protected int mesNac;
	/** Año de nacimiento del objeto.*/	protected int anioNac;
	/** Estatura del objeto.*/		protected double estatura;
	/** Tipo de sangre del objeto.*/	protected String tipoSangre;
	/** RH del objeto.*/			protected String rh;
	
	/*------------------------------------------------------------------------*/
	
	//Métodos creadores

	/**
	 *
	 * <!--Persona-->
	 *
	 * <b>Precondición:</b>		true.<br>
	 *
	 * <b>Poscondición:</b> 	Se ha generado un nuevo objeto de tipo persona.<br>
	 *				Todos sus atributos han sido inicializados.<br>
	 *
	 **/

	public Persona(){
		this.id = 0;
		this.tipoId = "";
		this.primerNombre = "";
	 	this.segundoNombre = "";
	 	this.primerApellido = "";
	 	this.segundoApellido = "";
		this.genero = "";
	 	this.nacionalidad = "";
		this.estadoCivil = "";
	 	this.diaNac = 0;
	 	this.mesNac = 0;
	 	this.anioNac = 0;
	 	this.estatura = 0.0;
	 	this.tipoSangre = "";
		this.rh = "";
	}

	/**
	 * 
	 * <!--Persona-->
	 *
	 * <b>Precondición:</b>		true.<br> 	
	 *
	 * <b>Poscondición:</b> 	Se ha generado un nuevo objeto de tipo persona.<br>
	 *				Todos sus atributos han sido inicializados.<br>
	 * @param 			primerNombre Primer nombre
	 * @param 			primerApellido Primer apellido
	 *
	 **/
	
	public Persona (String primerNombre, String primerApellido){
		this.id = 0;
		this.tipoId = "";
		this.primerNombre = primerNombre;
		this.segundoNombre = "";
	 	this.primerApellido = primerApellido;
	 	this.segundoApellido = "";
		this.genero = "";
	 	this.nacionalidad = "";
		this.estadoCivil = "";
	 	this.diaNac = 0;
	 	this.mesNac = 0;
	 	this.anioNac = 0;
	 	this.estatura = 0.0;
	 	this.tipoSangre = "";
		this.rh = "";	
	}
	
	/**
	 *
	 * <!--Persona-->
	 *
	 * <b>Precondición:</b>		true.<br> 	
	 *
	 * <b>Poscondición:</b> 	Se ha generado un nuevo objeto de tipo persona.<br>
	 *				Todos sus atributos han sido inicializados.<br>
	 *
	 * @param		id ID del objeto.
	 * @param		tipoId Tipo de ID del objeto.
	 * @param		primerNombre Primer nombre del objeto.
	 * @param		primerApellido Primer apellido del objeto.
	 * @param		genero Género del objeto
	 * @param		nacionalidad Nacionalidad del objeto.
	 **/
	
	public Persona (int id, String tipoId, String primerNombre, String primerApellido,
			String genero, String nacionalidad){
		this.id = id;
		this.tipoId = tipoId;
		this.primerNombre = primerNombre;
	 	this.segundoNombre = "";
	 	this.primerApellido = primerApellido;
	 	this.segundoApellido = "";
		this.genero = genero;
	 	this.nacionalidad = nacionalidad;
		this.estadoCivil = "";
	 	this.diaNac = 0;
	 	this.mesNac = 0;
	 	this.anioNac = 0;
		this.estatura = 0.0;
	 	this.tipoSangre = "";
		this.rh = "";	
	}
	
	
//	AQUÍ DEBES HACER UN MÉTODO CREADOR DONDE ENTREN COMO
//	PARÁMETRO TODOS LOS NUEVOS DATOS PARA INCIALIZAR TODOS LOS ATRIBUTOS

	/**
	*
	* <!--Persona-->
	*
	* <b>Precondición:</b>		true.<br>
	* <b>Poscondición:</b>		Se ha generado un nuevo objeto de tipo Persona.<br>
	*				Todos sus atributos han sido instanciados.
	*
	* @param 			id ID del objeto.
	* @param			tipoId Tipo de ID del objeto.
	* @param			primerNombre Primer nombre del objeto.
	* @param			segundoNombre Segundo nombre del objeto.
	* @param			primerApellido Primer apellido del objeto.
	* @param			segundoApellido Segundo apellido del objeto.
	* @param			genero Género del objeto.
	* @param			nacionalidad Nacionalidad del objeto.
	* @param			estadoCivil Estado civil del objeto.
	* @param			diaNac Día de nacimiento del objeto.
	* @param			mesNac Mes de nacimiento del objeto.
	* @param			anioNac Año de nacimiento del objeto.
	* @param			estatura Estatura del objeto.
	* @param			tipoSangre Tipo de sangre del objeto.
	* @param			rh RH del objeto.
	*
	**/
	
	
	public Persona(int id, String tipoId, String primerNombre, String segundoNombre,
			String primerApellido, String segundoApellido, String genero,
			String nacionalidad, String estadoCivil, int diaNac, int mesNac,
			int anioNac, double estatura, String tipoSangre, String rh){
		this.id = id;
		this.tipoId = tipoId;
		this.primerNombre = primerNombre;
	 	this.segundoNombre = segundoNombre;
	 	this.primerApellido = primerApellido;
	 	this.segundoApellido = segundoApellido;
		this.genero = genero;
	 	this.nacionalidad = nacionalidad;
		this.estadoCivil = estadoCivil;
	 	this.diaNac = diaNac;
	 	this.mesNac = mesNac;
	 	this.anioNac = anioNac;
		this.estatura = estatura;
	 	this.tipoSangre = tipoSangre;
		this.rh = rh;	
	}
	
	/*------------------------------------------------------------------------*/
	
	//Métodos analizadores
			
	/**
	 *
	 * <!--getPrimerNombre-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se devuelve el valor del atributo "primerNombre".<br>
	 *
	 * @return		Primer nombre del objeto.
	 *
	 **/

	public String getPrimerNombre(){
	 	return this.primerNombre;
	 }
	 
	 
	 //REALIZAR AQUÍ UN MÉTODO ANALIZADOR POR CADA UNO DE LOS ATRIBUTOS
	 
	 /**
	  *
	  * <!--getId-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "ID".<br>
	  *
	  * @return		ID del objeto.
	  *
	  **/

	public int getId(){
		return this.id;
	}

	 /**
	  * <!--getTipoId-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "tipoId".<br>
	  *
	  * @return		Tipo de ID del objeto.
	  *
	  **/
	 
	public String getTipoId(){
		return this.tipoId;
	}

	 /**
	  *
	  * <!--getSegundoNombre-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "segundoNombre".<br>
	  *
	  * @return		Segundo nombre del objeto.
	  *
	  **/

	public String getSegundoNombre(){
		return this.segundoNombre;
	}

	 /**
	  * 
	  * <!--getPrimerApellido-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "primerApellido".<br>
	  *
	  * @return		Primer apellido del objeto.
	  *
	  **/

	public String getPrimerApellido(){
		return this.primerApellido;
	}

	 /**
	  *
	  * <!--getSegundoApellido-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "segundoApellido".<br>
	  *
	  * @return		Segundo apellido del objeto.
	  *
	  **/

	public String getSegundoApellido(){
		return this.segundoApellido;
	}

	 /**
	  * 
	  * <!--getGenero-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "genero".<br>
	  *
	  * @return		Género del objeto.
	  *
	  **/

	public String getGenero(){
		return this.genero;
	}

	 /**
	  *
	  * <!--getNacionalidad-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "nacionalidad".<br>
	  *
	  * @return		Nacionalidad del objeto.
	  *
	  **/

	public String getNacionalidad(){
		return this.nacionalidad;
	}

	 /**
	  * 
	  * <!--getEstadoCivil-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "estadoCivil".<br>
	  *
	  * @return		Estado civil del objeto.
	  *
	  **/

	public String getEstadoCivil(){
		return this.estadoCivil;
	}

	 /**
	  * 
	  * <!--getDiaNac-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "diaNac".<br>
	  *
	  * @return		Día de nacimiento del objeto.
	  *
	  **/

	public int getDiaNac(){
		return this.diaNac;
	}

	 /**
	  *
	  * <!--getMesNac-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "mesNac".<br>
	  *
	  * @return		Mes de nacimiento del objeto.
	  *
	  **/

	public int getMesNac(){
		return this.mesNac;
	}

	 /**
	  * 
	  * <!--getAnioNac-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "anioNac".<br>
	  *
	  * @return		Año de nacimiento del objeto.
	  *
	  **/

	public int getAnioNac(){
		return this.anioNac;
	}

	 /**
	  * 
	  * <!--getEstatura-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "estatura".<br>
	  *
	  * @return		Estatura del objeto.
	  *
	  **/

	public double getEstatura(){
		return this.estatura;
	}

	 /**
	  * 
	  * <!--getTipoSangre-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "tipoSangre".<br>
	  *
	  * @return		Tipo de sangre del objeto.
	  *
	  **/

	public String getTipoSangre(){
		return this.tipoSangre;
	}

	 /**
	  * 
	  * <!--getRh-->
	  *
	  * <b>Precondición:</b>	El objeto persona debe existir.<br>
	  *
	  * <b>Poscondición:</b>	Se devuelve el valor del atributo "rh".<br>
	  *
	  * @return		RH del objeto.
	  *
	  **/

	public String getRh(){
		return this.rh;
	}


	 /*------------------------------------------------------------------------*/
	 
	 
	 //Métodos Modificadores
	 	 
	 /**
	 * 
	 * <!--setPrimerNombre-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "primerNombre"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param primerNombre Primer nombre del objeto.
	 *
	 **/

	 public void setPrimerNombre(String primerNombre){
	 	this.primerNombre = primerNombre;
	 }
	 
	 
	 //REALIZAR AQUÍ UN MÉTODO MODIFICADOR POR CADA UNO DE LOS ATRIBUTOS

	 /**
	 * 
	 * <!--setId-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "id"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param id ID del objeto.
	 *
	 **/

	 public void setId(int id){
		 this.id = id;
	 }
	 
	 /**
	 * 
	 * <!--setTipoId-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "tipoId"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param tipoId Tipo de ID del objeto.
	 *
	 **/
	 
	 public void setTipoId(String tipoId){
		this.tipoId = tipoId;
	 }

	 /**
	 * 
	 * <!--setSegundoNombre -->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "segundoNombre"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param segundoNombre Segundo nombre del objeto,
	 *
	 **/

	public void setSegundoNombre(String segundoNombre){
		this.segundoNombre = segundoNombre;
	}

	 /**
	 * 
	 * <!--setPrimerApellido-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "primerApellido"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param primerApellido Primer apellido del objeto.
	 *
	 **/

	public void setPrimerApellido(String primerApellido){
		this.primerApellido = primerApellido;
	}

	 /**
	 * 
	 * <!--setSegundoApellido-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "segundoApellido"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param segundoApellido Segundo apellido del objeto.
	 *
	 **/

	public void setSegundoApellido(String segundoApellido){
		this.segundoApellido = segundoApellido;
	}

	 /**
	 * 
	 * <!--setGenero-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "genero"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param genero Género del objeto.
	 *
	 **/

	public void setGenero(String genero){
		this.genero = genero;
	}

	 /**
	 * 
	 * <!--setNacionalidad-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "nacionalidad"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param nacionalidad Nacionalidad del objeto.
	 *
	 **/

	public void setNacionalidad(String nacionalidad){
		this.nacionalidad = nacionalidad;
	}

	 /**
	 * 
	 * <!--setEstadoCivil-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "estadoCivil"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param estadoCivil Estado civil del objeto.
	 *
	 **/

	public void setEstadoCivil(String estadoCivil){
		this.estadoCivil = estadoCivil;
	}

	 /**
	 * 
	 * <!--setDiaNac-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "diaNac"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param diaNac Día de nacimiento del objeto.
	 *
	 **/
	
	public void setDiaNac(int diaNac){
		this.diaNac = diaNac;
	}

	 /**
	 * 
	 * <!--setMesNac-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "mesNac"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param mesNac Mes de nacimiento del objeto.
	 *
	 **/

	public void setMesNac(int mesNac){
		this.mesNac = mesNac;
	}

	 /**
	 * 
	 * <!--setAnioNac-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "anioNac"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param anioNac Año de nacimiento del objeto.
	 *
	 **/

	public void setAnioNac(int anioNac){
		this.anioNac = anioNac;
	}

	 /**
	 * 
	 * <!--setEstatura-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "estatura"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param estatura Estatura del objeto.
	 *
	 **/

	public void setEstatura(double estatura){
		this.estatura = estatura;
	}

	 /**
	 * 
	 * <!--setTipoSangre-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	se asigna o se modifica el valor del atributo "tipoSangre"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param tipoSangre Tipo de sangre del objeto.
	 *
	 **/

	public void setTipoSangre(String tipoSangre){
		this.tipoSangre = tipoSangre;
	}

	 /**
	 * 
	 * <!--setRh-->
	 *
	 * <b>Precondición:</b>	El objeto persona debe existir.<br>
	 *
	 * <b>Poscondición:</b> 	Se asigna o se modifica el valor del atributo "rh"
	 * 			por el valor del parametro ingresado.
	 *
	 * @param rh RH del objeto.
	 *
	 **/

	public void setRh(String rh){
		this.rh = rh;
	}
}
